﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net.Http;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace APP_API
{
	#region Classes
	public class alumnes
	{		
		public string nom { get; set; }
		public string cognoms { get; set; }
		public string mail { get; set; }
		public string email;
		public string id;
	}
	public class assignatures
	{
		public string id;
		public string nom { get; set; }
		public string professor { get; set; }
	}
	public class alumnesAssignatures
	{
		public string alumne_id { get; set; }
		public string assignatura_id { get; set; }
	}
	public class notes
	{
		public string assignaturaID { get; set; }
		public string AlumneId { get; set; }
		public string nota { get; set; }
		public string updatedAt { get; set; }
	}
	public class alumneinfo
	{
		//class alumnes
		public string id;
		public string nom { get; set; }
		public string cognoms { get; set; }
		public string mail { get; set; }
		public string[,] infoAssignatures = new string[20,20];

		public alumneinfo(int n)
		{
			
		}
	}
	public class clnotes
	{
		public string alumne_id { get; set; }
		public string assignatura_id { get; set; }
		public string nota { get; set; }
	}
	#endregion
	public class ApiLink
	{
		List<List<string>> myList = new List<List<string>>();
		public List<alumnes> itAlu;
		public List<assignatures> itAss;
		public alumneinfo info;

		private HttpClient client = new HttpClient();
		private string base_url = "http://127.0.0.1:3000/";
		public ApiLink()
		{
			try
			{
				client.DefaultRequestHeaders.Add("x-token", "C0UsWlYxXrMx81TKN2Eq");
			}
			catch(Exception e)
			{
				Console.WriteLine(e);
			}
		}
		#region ALUMNES
		public async Task<string> getAlumne()
		{
			try
			{
				HttpResponseMessage response = await client.GetAsync(base_url + "api/v1/alumnes");
				response.EnsureSuccessStatusCode();
				string responseBody = await response.Content.ReadAsStringAsync();
				itAlu = JsonConvert.DeserializeObject<List<alumnes>>(responseBody.ToString());
				return responseBody;
			}
			catch(Exception e)
			{
				return null;
			}
		}
		public async Task<string> getAlumneById(String id)
		{
			try
			{
				HttpResponseMessage response = await client.GetAsync(base_url + "api/v1/alumne/" + id);
				response.EnsureSuccessStatusCode();
				string responseBody = await response.Content.ReadAsStringAsync();
				JToken token = JToken.Parse(responseBody);
				JArray assign = (JArray)token.SelectToken("Assignaturas");
				JArray notes = (JArray)token.SelectToken("Assignaturas[0].Notes");
				
				info = new alumneinfo(assign.Count);

				info.id = (String)token.SelectToken("id");
				info.nom = (String)token.SelectToken("nom");
				info.cognoms = (String)token.SelectToken("cognoms");
				info.mail = (String)token.SelectToken("mail"); ;
				
				int countAssign = 0;
				foreach (JObject itemAssign in assign)
				{
					info.infoAssignatures[countAssign, 0] = (String)token.SelectToken("Assignaturas["+countAssign+"].nom");
					int countNotes = 0;
					foreach (JObject itemNotes in notes)
					{
						string x = (String)token.SelectToken("Assignaturas[" + countAssign + "].Notes[" + countNotes + "].AlumneId");
						if(x == info.id)
						{
							info.infoAssignatures[countAssign, countNotes + 1] = (String)token.SelectToken("Assignaturas[" + countAssign + "].Notes[" + countNotes + "].nota");
							countNotes++;
						}
					}			
					countAssign=countAssign+1;
				}
				//JArray assignaturesAlumne = (JArray)token.SelectToken("Assignaturas[0].AssignaturaAlumne");
				//String nota = (String)token.SelectToken("Assignaturas[0].Notes[0].nota");
				//JArray notes = (JArray)token.SelectToken("Assignaturas[0].Notes");
				//for (int i = 0; i < notes.Count; i++)				
				//listNotes = JsonConvert.DeserializeObject<List<string>>(notes.ToString());
				//info.notes[i].AlumneId = notes.Children().Select();
				return responseBody;
			}
			catch (Exception e)
			{
				MessageBox.Show(e.ToString());
				return null;
			}
		}
		public async Task postAlumne(alumnes al)
		{			
				var stringContent = new StringContent(JsonConvert.SerializeObject(al), Encoding.UTF8);
				try
				{
					stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					HttpResponseMessage response = await client.PostAsync(base_url + "api/v1/alumne", stringContent);
					response.EnsureSuccessStatusCode();
				MessageBox.Show("inserit correctament l'alumne " + al.nom + " " + al.cognoms + " amb correu" + al.email);
				}
				catch (Exception e)
				{
				MessageBox.Show("error al inserir l'alumne " + al.nom +" "+ al.cognoms + " comrpova que el correu " + al.mail + " no estigui repetit");
				}
		}
		public async Task deleteAlumne(alumnes alu)
		{
			var stringContent = new StringContent(JsonConvert.SerializeObject(alu), Encoding.UTF8);
			try
			{
				stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
				HttpResponseMessage response = await client.DeleteAsync(base_url + "api/v1/alumne/"+alu.id);
				response.EnsureSuccessStatusCode();
				MessageBox.Show("eliminat l'alumne " + alu.id );
			}
			catch (Exception e)
			{
				MessageBox.Show("error al eliminar l'alumne " + alu.id + " que correspont a " + alu.nom + " " + alu.cognoms);
			}
		}
		#endregion

		#region ASSIGNATURES
		public async Task<string> getAssignatura()
		{
			try
			{
				HttpResponseMessage response = await client.GetAsync(base_url + "api/v1/assignatures");
				response.EnsureSuccessStatusCode();
				string responseBody = await response.Content.ReadAsStringAsync();
				itAss = JsonConvert.DeserializeObject<List<assignatures>>(responseBody.ToString());
				return responseBody;
			}
			catch (Exception e)
			{
				return null;
			}
		}
		public async Task postAssignatura(assignatures ass)
		{

			var stringContent = new StringContent(JsonConvert.SerializeObject(ass), Encoding.UTF8);
			try
			{
				stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
				HttpResponseMessage response = await client.PostAsync(base_url + "api/v1/assignatura", stringContent);
				response.EnsureSuccessStatusCode();
				MessageBox.Show("Has inserit correctament l'assignatura " + ass.nom + " amb el professor " + ass.professor);
			}
			catch (Exception e)
			{
				MessageBox.Show("error al inserir l'assignatura " + ass.nom + " amb el professor " + ass.professor);
			}
		}
		#endregion

		#region ALUMNES-ASSIGNATURES
		public async Task postAlumnesAssignatures(alumnesAssignatures AssAlu)
		{

			var stringContent = new StringContent(JsonConvert.SerializeObject(AssAlu), Encoding.UTF8);
			try
			{
				stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
				HttpResponseMessage response = await client.PostAsync(base_url + "api/v1/vincular", stringContent);
				response.EnsureSuccessStatusCode();
				MessageBox.Show("Has inserit correctament la vinculacio de l'alumne " + AssAlu.alumne_id + " amb l'assignatura " + AssAlu.assignatura_id);
			}
			catch (Exception e)
			{
				MessageBox.Show("error");
			}
		}
		#endregion
		public async Task postNota(clnotes nota)
		{
			var stringContent = new StringContent(JsonConvert.SerializeObject(nota), Encoding.UTF8);
			try
			{
				stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
				HttpResponseMessage response = await client.PostAsync(base_url + "api/v1/nota", stringContent);
				response.EnsureSuccessStatusCode();
				MessageBox.Show("inserit correctament l'alumne " + nota.alumne_id + " a l'assignatura " + nota.assignatura_id + " amb la nota" + nota.nota);
			}
			catch (Exception e)
			{
				MessageBox.Show("error al inserir l'alumne " + nota.alumne_id + " a l'assignatura " + nota.assignatura_id + " amb la nota" + nota.nota);
			}
		}

	}
}
