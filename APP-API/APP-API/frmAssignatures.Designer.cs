﻿namespace APP_API
{
	partial class frmAssignatures
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.resetDGV = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btnGet = new System.Windows.Forms.Button();
			this.btnPost = new System.Windows.Forms.Button();
			this.dgvJson = new System.Windows.Forms.DataGridView();
			this.btnSelArxiu = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvJson)).BeginInit();
			this.SuspendLayout();
			// 
			// resetDGV
			// 
			this.resetDGV.Location = new System.Drawing.Point(691, 55);
			this.resetDGV.Name = "resetDGV";
			this.resetDGV.Size = new System.Drawing.Size(75, 23);
			this.resetDGV.TabIndex = 13;
			this.resetDGV.Text = "Reset DGV";
			this.resetDGV.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.btnGet);
			this.groupBox1.Controls.Add(this.btnPost);
			this.groupBox1.Location = new System.Drawing.Point(772, 55);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(400, 156);
			this.groupBox1.TabIndex = 12;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Controls API";
			// 
			// btnGet
			// 
			this.btnGet.Location = new System.Drawing.Point(6, 31);
			this.btnGet.Name = "btnGet";
			this.btnGet.Size = new System.Drawing.Size(75, 23);
			this.btnGet.TabIndex = 1;
			this.btnGet.Text = "GET";
			this.btnGet.UseVisualStyleBackColor = true;
			this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
			// 
			// btnPost
			// 
			this.btnPost.Location = new System.Drawing.Point(99, 31);
			this.btnPost.Name = "btnPost";
			this.btnPost.Size = new System.Drawing.Size(75, 23);
			this.btnPost.TabIndex = 2;
			this.btnPost.Text = "Post";
			this.btnPost.UseVisualStyleBackColor = true;
			this.btnPost.Click += new System.EventHandler(this.btnPost_Click);
			// 
			// dgvJson
			// 
			this.dgvJson.AllowUserToAddRows = false;
			this.dgvJson.BackgroundColor = System.Drawing.Color.White;
			this.dgvJson.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvJson.Location = new System.Drawing.Point(18, 96);
			this.dgvJson.Name = "dgvJson";
			this.dgvJson.Size = new System.Drawing.Size(748, 442);
			this.dgvJson.TabIndex = 11;
			// 
			// btnSelArxiu
			// 
			this.btnSelArxiu.Location = new System.Drawing.Point(480, 53);
			this.btnSelArxiu.Name = "btnSelArxiu";
			this.btnSelArxiu.Size = new System.Drawing.Size(135, 23);
			this.btnSelArxiu.TabIndex = 10;
			this.btnSelArxiu.Text = "Seleccionar Arxiu Json";
			this.btnSelArxiu.UseVisualStyleBackColor = true;
			this.btnSelArxiu.Click += new System.EventHandler(this.btnSelArxiu_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(18, 55);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(456, 20);
			this.textBox1.TabIndex = 9;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(172, 31);
			this.label1.TabIndex = 14;
			this.label1.Text = "Assignatures";
			// 
			// frmAssignatures
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.SkyBlue;
			this.ClientSize = new System.Drawing.Size(1184, 550);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.resetDGV);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.dgvJson);
			this.Controls.Add(this.btnSelArxiu);
			this.Controls.Add(this.textBox1);
			this.Name = "frmAssignatures";
			this.Text = "Assignatures";
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvJson)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button resetDGV;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button btnGet;
		private System.Windows.Forms.Button btnPost;
		private System.Windows.Forms.DataGridView dgvJson;
		private System.Windows.Forms.Button btnSelArxiu;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label1;
	}
}