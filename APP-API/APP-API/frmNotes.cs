﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APP_API
{
	public partial class frmNotes : Form
	{
		public frmNotes()
		{
			InitializeComponent();
			carragarDGVAssignatures();
			carragarDGVAlumnes();
		}
		public void carragarDGVAssignatures()
		{
			List<assignatures> items;
			ApiLink api = new ApiLink();
			txtNota.Text = "";
			dgvAssignatures.Rows.Clear();
			MessageBox.Show("Espera uns segons, s'esta realitzant la consulta a assignatures" + api.getAssignatura().ToString());
			items = api.itAss;
			readApiResponse(items);
		}
		public void readApiResponse(List<assignatures> item)
		{
			try
			{
				dgvAssignatures.Columns.Add("id", "id");
				dgvAssignatures.Columns.Add("nom", "nom");
				dgvAssignatures.Columns.Add("professor", "professor");
				int a = 0;
				if (a == 0)
				{
					for (int i = 0; i < item.Count; i++)
					{
						dgvAssignatures.Rows.Add(item[i].id,item[i].nom, item[i].professor);
						dgvAssignatures.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
					}
				}
			}
			catch (Exception e)
			{
				MessageBox.Show("Error en el get. Tens l'appi encesa?");
			}
		}

		public void carragarDGVAlumnes()
		{
			List<alumnes> items;
			ApiLink api = new ApiLink();
			txtNota.Text = "";
			dgvAlumnes.Rows.Clear();
			dgvAlumnes.Columns.Clear();
			MessageBox.Show("Espera un segons, s'esta realitzant la consulta a alumnes" + api.getAlumne().ToString());
			items = api.itAlu;
			readApiResponse(items);
		}
		public void readApiResponse(List<alumnes> item)
		{
			try
			{
				dgvAlumnes.Columns.Add("id", "id");
				dgvAlumnes.Columns.Add("nom", "nom");
				dgvAlumnes.Columns.Add("cognoms", "cognoms");
				dgvAlumnes.Columns.Add("email", "email");
				int a = 0;
				if (a == 0)
				{
					for (int i = 0; i < item.Count; i++)
					{
						dgvAlumnes.Rows.Add(item[i].id, item[i].nom, item[i].cognoms, item[i].mail);
						dgvAlumnes.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
					}
				}
				for (int i = 0; i < (dgvAlumnes.Rows.Count - 1); i++)
				{

					if (dgvAlumnes.Rows[i].Cells["nom"].Value == null ||
					   dgvAlumnes.Rows[i].Cells["cognoms"].Value == null || dgvAlumnes.Rows[i].Cells["email"].Value == null)
					{
						MessageBox.Show("columnes buides, estàs segur que estàs inserint el Json d'alumnes? si es així són necessaris els camps nom, cognoms i email.");
						dgvAlumnes.Rows.Clear();
					}
				}
				
			}
			catch (Exception e)
			{
				MessageBox.Show("Error en el format del JSON");
			}
		}
		private void dgvAlumnes_SelectionChanged(object sender, EventArgs e)
		{
			if(dgvAlumnes.SelectedRows.Count == 1)
			{
				int selectedrowindex = dgvAlumnes.SelectedCells[0].RowIndex;
				DataGridViewRow selectedRow = dgvAlumnes.Rows[selectedrowindex];
				txtAlumneId.Text = selectedRow.Cells["id"].Value.ToString();
				txtAlumneNom.Text = selectedRow.Cells["nom"].Value.ToString();
				txtAlumneCognom.Text = selectedRow.Cells["cognoms"].Value.ToString();
			}
		}

		private void dgvAssignatures_SelectionChanged(object sender, EventArgs e)
		{
			if (dgvAssignatures.SelectedRows.Count == 1)
			{
				int selectedrowindex = dgvAssignatures.SelectedCells[0].RowIndex;
				DataGridViewRow selectedRow = dgvAssignatures.Rows[selectedrowindex];
				txtIdAssignatura.Text = selectedRow.Cells["id"].Value.ToString();
				txtNomAssignatura.Text = selectedRow.Cells["nom"].Value.ToString();
				txtNomProfessor.Text = selectedRow.Cells["professor"].Value.ToString();
			}
		}

		private void btnAfegirNota_Click(object sender, EventArgs e)
		{
			ApiLink api = new ApiLink();

			if (txtAlumneId.Text != "" || txtIdAssignatura.Text != "")
			{
				if(txtNota.Text != "")
				{
					clnotes nota = new clnotes();
					//al.id = row.Cells["id"].Value.ToString();
					nota.alumne_id= txtAlumneId.Text;
					nota.assignatura_id = txtIdAssignatura.Text;
					nota.nota = txtNota.Text;
					api.postNota(nota);
				}
				else
				{
					MessageBox.Show("Has de escriure una nota");
				}
			}
			else
			{
				MessageBox.Show("has de seleccionar un alumne i un usuari");
			}
		}
	}
}
