﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APP_API
{
	public partial class frmAssignaturaAlumne : Form
	{
		

		public frmAssignaturaAlumne()
		{
			InitializeComponent();
			carragarDGVAlumnes();
			carragarDGVAssignatures();
		}
	#region ALUMNES
		#region API-ALUMNES
		public void carragarDGVAlumnes()
		{
			List<alumnes> items;
			ApiLink api = new ApiLink();
			dgvAlumnes.Rows.Clear();
			MessageBox.Show("Espera un segons, s'esta realitzant la consulta d'alumnes" + api.getAlumne().ToString());
			items = api.itAlu;
			readApiResponseAlumne(items);
		}
		public void readApiResponseAlumne(List<alumnes> item)
		{
			try
			{
				dgvAlumnes.Columns.Add("id", "id");
				dgvAlumnes.Columns.Add("nom", "nom");
				dgvAlumnes.Columns.Add("cognoms", "cognoms");
				dgvAlumnes.Columns.Add("email", "email");
				int a = 0;
				if (a == 0)
				{
					for (int i = 0; i < item.Count; i++)
					{
						dgvAlumnes.Rows.Add(item[i].id, item[i].nom, item[i].cognoms, item[i].mail);
						dgvAlumnes.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
					}
				}				
			}
			catch (Exception e)
			{
				MessageBox.Show("Error en el format del JSON");
			}
		}
		#endregion
		private void dgvAlumnes_SelectionChanged(object sender, EventArgs e)
		{
			if(dgvAlumnes.SelectedRows.Count == 1)
			{
				txtIdAlumne.Text = dgvAlumnes.SelectedRows[0].Cells[0].Value.ToString();
				txtNomAlumne.Text = dgvAlumnes.SelectedRows[0].Cells[1].Value.ToString() + " " +dgvAlumnes.SelectedRows[0].Cells[2].Value.ToString();
			}
			else
			{
				txtIdAlumne.Text = "Varis";
				txtNomAlumne.Text = "Tens " + dgvAlumnes.SelectedRows.Count.ToString()+ " alumnes seleccionats";
			}
		}
		#endregion

	#region ASSIGNATURES
		#region API-ASSIGNATURES
		public void carragarDGVAssignatures()
		{
			List<assignatures> items;
			ApiLink api = new ApiLink();
			dgvAssignatures.Rows.Clear();
			MessageBox.Show("Espera un segons, s'esta realitzant la consulta d'assignatures" + api.getAssignatura().ToString());
			items = api.itAss;
			readApiResponseAssignatura(items);
		}
		public void readApiResponseAssignatura(List<assignatures> item)
		{
			try
			{
				dgvAssignatures.Columns.Add("id", "id");
				dgvAssignatures.Columns.Add("nom", "nom");
				dgvAssignatures.Columns.Add("professor", "professor");
				int a = 0;
				if (a == 0)
				{
					for (int i = 0; i < item.Count; i++)
					{

						dgvAssignatures.Rows.Add(item[i].id, item[i].nom, item[i].professor);
						dgvAssignatures.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
					}
				}
			}
			catch (Exception e)
			{
				MessageBox.Show("Error en el get. Tens l'appi encesa?");
			}
		}
		#endregion
		private void dgvAssignatures_SelectionChanged(object sender, EventArgs e)
		{
			if (dgvAssignatures.SelectedRows.Count == 1)
			{
				txtIdAssignatura.Text = dgvAssignatures.SelectedRows[0].Cells[0].Value.ToString();
				txtNomAssignatura.Text = dgvAssignatures.SelectedRows[0].Cells[1].Value.ToString();
			}
			else
			{
				txtIdAssignatura.Text = "Varis";
				txtNomAssignatura.Text = "Tens " + dgvAssignatures.SelectedRows.Count.ToString() + " assignatures seleccionades";
			}
		}


		#endregion

		private void btnVincular_Click(object sender, EventArgs e)
		{
			ApiLink api = new ApiLink();
			if(dgvAlumnes.SelectedRows.Count != 0 && dgvAssignatures.SelectedRows.Count != 0)
			{
				foreach (DataGridViewRow rAlu in dgvAlumnes.SelectedRows)
				{
					foreach (DataGridViewRow rAss in dgvAssignatures.SelectedRows)
					{
						alumnesAssignatures AssAlu = new alumnesAssignatures();
						AssAlu.alumne_id = rAlu.Cells["id"].Value.ToString();
						AssAlu.assignatura_id = rAss.Cells["id"].Value.ToString();
						api.postAlumnesAssignatures(AssAlu);
					}
				}
			}
			
		}

	}
}
