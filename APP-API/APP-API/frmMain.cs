﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APP_API
{

	public partial class frmMain : Form
	{

		public frmMain()
		{
			InitializeComponent();
		}

		private void btnMenu_Click(object sender, EventArgs e)
		{
			if (PanelLeftMenu.Width == 45)
			{
				
				PanelLeftMenu.Visible = false;
				PanelLeftMenu.Width = 250;
				bunifuTransition1.ShowSync(PanelLeftMenu);
				bTransitionLogo.ShowSync(pictureBox1);
				btnConfiguracio.Text = "Made By Roger";
				bTransitionLogo.ShowSync(btnConfiguracio);
			}
			else
			{				
				bTransitionLogo.HideSync(pictureBox1);
				btnConfiguracio.Text = "";
				bTransitionLogo.ShowSync(btnConfiguracio);
				PanelLeftMenu.Width = 45;
				PanelLeftMenu.Visible = false;
				bunifuTransition1.ShowSync(PanelLeftMenu);
				
			}
		}
		private void btnExit_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void btnMinimize_Click(object sender, EventArgs e)
		{
			WindowState = FormWindowState.Minimized;
		}

		private void btnMaximize_Click(object sender, EventArgs e)
		{
			if (WindowState == FormWindowState.Maximized)
			{
				WindowState = FormWindowState.Normal;
			}
			else
			{
				MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
				WindowState = FormWindowState.Maximized;
			}
		}   

		private void btnAlumnes_Click(object sender, EventArgs e)
		{
			panel1.Controls.Clear();
			frmAlumnes frm = new frmAlumnes();
			frm.TopLevel = false;
			frm.AutoScroll = true;
			this.panel1.Controls.Add(frm);
			frm.FormBorderStyle = FormBorderStyle.None;
			frm.Dock = DockStyle.Fill;
			frm.Show();
		}

		private void btnAssignatures_Click(object sender, EventArgs e)
		{
			panel1.Controls.Clear();
			frmAssignatures frm = new frmAssignatures();
			frm.TopLevel = false;
			frm.AutoScroll = true;
			this.panel1.Controls.Add(frm);
			frm.FormBorderStyle = FormBorderStyle.None;
			frm.Dock = DockStyle.Fill;
			frm.Show();
		}

		private void btnVincular_Click(object sender, EventArgs e)
		{
			panel1.Controls.Clear();
			frmAssignaturaAlumne frm = new frmAssignaturaAlumne();
			frm.TopLevel = false;
			frm.AutoScroll = true;
			this.panel1.Controls.Add(frm);
			frm.FormBorderStyle = FormBorderStyle.None;
			frm.Dock = DockStyle.Fill;
			frm.Show();
		}

		private void btnBunExit_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void btnAgenda_Click(object sender, EventArgs e)
		{
			panel1.Controls.Clear();
			frmNotes frm = new frmNotes();
			frm.TopLevel = false;
			frm.AutoScroll = true;
			this.panel1.Controls.Add(frm);
			frm.FormBorderStyle = FormBorderStyle.None;
			frm.Dock = DockStyle.Fill;
			frm.Show();
		}
	}
}




