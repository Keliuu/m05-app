﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace APP_API
{
	public partial class frmAlumnes : Form
	{
		ApiLink api = new ApiLink();
		public int postCount = 0;

		public frmAlumnes()
		{
			InitializeComponent();
		}

		public void ReadtxtJson(string path)
		{
			List<alumnes> items;
			using (StreamReader r = new StreamReader(path))
			{
				try
				{
					string json = r.ReadToEnd();
					items = JsonConvert.DeserializeObject<List<alumnes>>(json);
					dgvJson.Columns.Add("nom", "nom");
					dgvJson.Columns.Add("cognoms", "cognoms");
					dgvJson.Columns.Add("email", "email");
					int a = 0;
					if (a == 0)
					{
						for (int i = 0; i < items.Count; i++)
						{
							dgvJson.Rows.Add(items[i].nom, items[i].cognoms, items[i].email);
						}
					}
					for (int i = 0; i < (dgvJson.Rows.Count - 1); i++)
					{
						
						if (dgvJson.Rows[i].Cells["nom"].Value == null ||
						   dgvJson.Rows[i].Cells["cognoms"].Value == null || dgvJson.Rows[i].Cells["email"].Value == null)
						{							
							MessageBox.Show("columnes buides, estàs segur que estàs inserint el Json d'alumnes? si es així són necessaris els camps nom, cognoms i email.");
							dgvJson.Rows.Clear();
						}
					}
					comprovarDelete();
				}
				catch (Exception e)
				{
					MessageBox.Show("Error en el format del JSON");
					textBox1.Text = "";
				}
			}			
		}
		public void readApiResponse(List<alumnes> item) {
			try
			{
				dgvJson.Columns.Add("id", "id");
				dgvJson.Columns.Add("nom", "nom");
				dgvJson.Columns.Add("cognoms", "cognoms");
				dgvJson.Columns.Add("email", "email");
				int a = 0;
				if (a == 0)
				{
					for (int i = 0; i < item.Count; i++)
					{
						dgvJson.Rows.Add(item[i].id,item[i].nom, item[i].cognoms, item[i].mail);
						dgvJson.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
					}
				}
				for (int i = 0; i < (dgvJson.Rows.Count - 1); i++)
				{

					if (dgvJson.Rows[i].Cells["nom"].Value == null ||
					   dgvJson.Rows[i].Cells["cognoms"].Value == null || dgvJson.Rows[i].Cells["email"].Value == null)
					{
						MessageBox.Show("columnes buides, estàs segur que estàs inserint el Json d'alumnes? si es així són necessaris els camps nom, cognoms i email.");
						dgvJson.Rows.Clear();
					}
				}
				comprovarDelete();
			}
			catch (Exception e)
			{
				MessageBox.Show("Error en el format del JSON");
			}		
		}
		public void readApiInfoAlumne(alumneinfo alInf)
		{
			dgvAssignatures.Rows.Clear();
			dgvAssignatures.Columns.Clear();
			try
			{
				txtAluNom.Text = alInf.nom;
				txtAluCognom.Text = alInf.cognoms;
				txtAluEmail.Text = alInf.mail;
				dgvAssignatures.Columns.Add("Informacio Assignatures", "Informacio Assignatures");
				dgvAssignatures.Columns["Informacio Assignatures"].SortMode = DataGridViewColumnSortMode.NotSortable;
				int countAss = 0;
				foreach (String ass in alInf.infoAssignatures)
				{
					if(ass != null)
					{
							dgvAssignatures.Rows.Add(ass);
							countAss++;					
					}
				}
				
			}
			catch (Exception e)
			{
				MessageBox.Show("Error en el format del JSON");
			}
		}
		private void btnSelectArxiu_Click(object sender, EventArgs e)
		{
			dgvJson.Rows.Clear();
			OpenFileDialog openFileDialog1 = new OpenFileDialog();

			openFileDialog1.InitialDirectory = "H:\\Keliu\\Asix\\M05-Hardware\\practicaExamenFinal";
			//openFileDialog1.Filter = "Database files (*.mdb, *.accdb)|*.mdb;*.accdb";
			openFileDialog1.FilterIndex = 0;
			openFileDialog1.RestoreDirectory = true;

			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				textBox1.Text = openFileDialog1.FileName;
				ReadtxtJson(textBox1.Text);
			}
		}
		private void button1_Click(object sender, EventArgs e)
		{
			dgvJson.Rows.Clear();
		}
		//Funcions API
		private void btnGet_Click(object sender, EventArgs e)
		{
			List<alumnes> items;
			ApiLink api = new ApiLink();
			textBox1.Text = "";
			dgvJson.Rows.Clear();
			dgvJson.Columns.Clear();
			MessageBox.Show("Espera un segons, s'esta realitzant la consulta a alumnes" + api.getAlumne().ToString());
			items = api.itAlu;
			readApiResponse(items);
		}

		private void btnPost_Click(object sender, EventArgs e)
		{
			if (dgvJson.Rows.Count != 0)
			{
				foreach (DataGridViewRow row in dgvJson.Rows)
				{
					if (row.DefaultCellStyle.BackColor != Color.LightBlue)
					{
						alumnes al = new alumnes();
						//al.id = row.Cells["id"].Value.ToString();
						al.nom = row.Cells["nom"].Value.ToString();
						al.cognoms = row.Cells["cognoms"].Value.ToString();
						al.mail = row.Cells["email"].Value.ToString();
						api.postAlumne(al);
						if (comprovarExiseix(al) != true)
						{
							
						}
						else
						{
							MessageBox.Show("Error, no s'ha pogut inserir l'alumne " + al.nom + " " + al.cognoms + " perquè ja existeix");
						}
					}
				}

			}
			else
			{
				MessageBox.Show("error, no pots fer post perquè no tens cap registre a inserir");
			}
			
		}
		public void comprovarDelete()
		{
			Boolean deleteVisible = false;
			foreach (DataGridViewRow r in dgvJson.Rows)
			{
				if (r.DefaultCellStyle.BackColor == Color.LightBlue)
				{
					deleteVisible = true;
				}
				else
				{
					deleteVisible = false;
				}
			}
			btnDelete.Enabled = deleteVisible;
		}
		private void btnDelete_Click(object sender, EventArgs e)
		{
			if (dgvJson.SelectedRows.Count != 0)
			{
				foreach (DataGridViewRow rAlu in dgvJson.SelectedRows)
				{
					alumnes Alu = new alumnes();
					Alu.id = rAlu.Cells["id"].Value.ToString();
					Alu.nom = rAlu.Cells["nom"].Value.ToString();
					Alu.cognoms = rAlu.Cells["cognoms"].Value.ToString();
					api.deleteAlumne(Alu);
					btnGet.PerformClick();
				}

			}
		}
		public Boolean comprovarExiseix(alumnes al)
		{			
			return false;
		}

		private void btnBuscarId_Click(object sender, EventArgs e)
		{
			alumneinfo items;
			MessageBox.Show("Espera un segons, s'esta realitzant la consulta a alumnes" + api.getAlumneById(txtGetId.Text).ToString());
			items = api.info;
			readApiInfoAlumne(items);
		}
	}
}
