﻿namespace APP_API
{
	partial class frmMain
	{
		/// <summary>
		/// Variable del diseñador necesaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpiar los recursos que se estén usando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de Windows Forms

		/// <summary>
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido de este método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
			BunifuAnimatorNS.Animation animation2 = new BunifuAnimatorNS.Animation();
			this.bunifuTransition1 = new BunifuAnimatorNS.BunifuTransition(this.components);
			this.panel1 = new System.Windows.Forms.Panel();
			this.PanelLeftMenu = new Bunifu.Framework.UI.BunifuGradientPanel();
			this.btnBunExit = new Bunifu.Framework.UI.BunifuFlatButton();
			this.btnConfiguracio = new Bunifu.Framework.UI.BunifuFlatButton();
			this.btnAgenda = new Bunifu.Framework.UI.BunifuFlatButton();
			this.btnVincular = new Bunifu.Framework.UI.BunifuFlatButton();
			this.btnAssignatures = new Bunifu.Framework.UI.BunifuFlatButton();
			this.btnAlumnes = new Bunifu.Framework.UI.BunifuFlatButton();
			this.panelMenuState = new System.Windows.Forms.Panel();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.btnMenu = new Bunifu.Framework.UI.BunifuImageButton();
			this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
			this.panelTopWindow = new Bunifu.Framework.UI.BunifuGradientPanel();
			this.btnbtnMinimized = new Bunifu.Framework.UI.BunifuImageButton();
			this.btnMaximized = new Bunifu.Framework.UI.BunifuImageButton();
			this.btnExit = new Bunifu.Framework.UI.BunifuImageButton();
			this.bWindowDrag = new Bunifu.Framework.UI.BunifuDragControl(this.components);
			this.bTransitionLogo = new BunifuAnimatorNS.BunifuTransition(this.components);
			this.PanelLeftMenu.SuspendLayout();
			this.panelMenuState.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnMenu)).BeginInit();
			this.panelTopWindow.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnbtnMinimized)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnMaximized)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
			this.SuspendLayout();
			// 
			// bunifuTransition1
			// 
			this.bunifuTransition1.AnimationType = BunifuAnimatorNS.AnimationType.Particles;
			this.bunifuTransition1.Cursor = null;
			animation1.AnimateOnlyDifferences = true;
			animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
			animation1.LeafCoeff = 0F;
			animation1.MaxTime = 1F;
			animation1.MinTime = 0F;
			animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
			animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
			animation1.MosaicSize = 1;
			animation1.Padding = new System.Windows.Forms.Padding(100, 50, 100, 150);
			animation1.RotateCoeff = 0F;
			animation1.RotateLimit = 0F;
			animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
			animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
			animation1.TimeCoeff = 2F;
			animation1.TransparencyCoeff = 0F;
			this.bunifuTransition1.DefaultAnimation = animation1;
			this.bunifuTransition1.MaxAnimationTime = 3000;
			this.bunifuTransition1.TimeStep = 0.01F;
			// 
			// panel1
			// 
			this.bTransitionLogo.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(250, 32);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1200, 589);
			this.panel1.TabIndex = 3;
			// 
			// PanelLeftMenu
			// 
			this.PanelLeftMenu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelLeftMenu.BackgroundImage")));
			this.PanelLeftMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.PanelLeftMenu.Controls.Add(this.btnBunExit);
			this.PanelLeftMenu.Controls.Add(this.btnConfiguracio);
			this.PanelLeftMenu.Controls.Add(this.btnAgenda);
			this.PanelLeftMenu.Controls.Add(this.btnVincular);
			this.PanelLeftMenu.Controls.Add(this.btnAssignatures);
			this.PanelLeftMenu.Controls.Add(this.btnAlumnes);
			this.PanelLeftMenu.Controls.Add(this.panelMenuState);
			this.bTransitionLogo.SetDecoration(this.PanelLeftMenu, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.PanelLeftMenu, BunifuAnimatorNS.DecorationType.None);
			this.PanelLeftMenu.Dock = System.Windows.Forms.DockStyle.Left;
			this.PanelLeftMenu.GradientBottomLeft = System.Drawing.Color.DodgerBlue;
			this.PanelLeftMenu.GradientBottomRight = System.Drawing.Color.DeepSkyBlue;
			this.PanelLeftMenu.GradientTopLeft = System.Drawing.Color.Blue;
			this.PanelLeftMenu.GradientTopRight = System.Drawing.Color.RoyalBlue;
			this.PanelLeftMenu.Location = new System.Drawing.Point(0, 32);
			this.PanelLeftMenu.Name = "PanelLeftMenu";
			this.PanelLeftMenu.Quality = 10;
			this.PanelLeftMenu.Size = new System.Drawing.Size(250, 589);
			this.PanelLeftMenu.TabIndex = 2;
			// 
			// btnBunExit
			// 
			this.btnBunExit.Activecolor = System.Drawing.Color.Transparent;
			this.btnBunExit.BackColor = System.Drawing.Color.Transparent;
			this.btnBunExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.btnBunExit.BorderRadius = 0;
			this.btnBunExit.ButtonText = "Sortir";
			this.btnBunExit.Cursor = System.Windows.Forms.Cursors.Hand;
			this.bTransitionLogo.SetDecoration(this.btnBunExit, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.btnBunExit, BunifuAnimatorNS.DecorationType.None);
			this.btnBunExit.DisabledColor = System.Drawing.Color.Gray;
			this.btnBunExit.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.btnBunExit.Iconcolor = System.Drawing.Color.Transparent;
			this.btnBunExit.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnBunExit.Iconimage")));
			this.btnBunExit.Iconimage_right = null;
			this.btnBunExit.Iconimage_right_Selected = null;
			this.btnBunExit.Iconimage_Selected = null;
			this.btnBunExit.IconMarginLeft = 5;
			this.btnBunExit.IconMarginRight = 0;
			this.btnBunExit.IconRightVisible = true;
			this.btnBunExit.IconRightZoom = 0D;
			this.btnBunExit.IconVisible = true;
			this.btnBunExit.IconZoom = 80D;
			this.btnBunExit.IsTab = false;
			this.btnBunExit.Location = new System.Drawing.Point(0, 499);
			this.btnBunExit.Name = "btnBunExit";
			this.btnBunExit.Normalcolor = System.Drawing.Color.Transparent;
			this.btnBunExit.OnHovercolor = System.Drawing.Color.DodgerBlue;
			this.btnBunExit.OnHoverTextColor = System.Drawing.Color.White;
			this.btnBunExit.selected = false;
			this.btnBunExit.Size = new System.Drawing.Size(250, 45);
			this.btnBunExit.TabIndex = 10;
			this.btnBunExit.Text = "Sortir";
			this.btnBunExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.btnBunExit.Textcolor = System.Drawing.Color.White;
			this.btnBunExit.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnBunExit.Click += new System.EventHandler(this.btnBunExit_Click);
			// 
			// btnConfiguracio
			// 
			this.btnConfiguracio.Activecolor = System.Drawing.Color.Transparent;
			this.btnConfiguracio.BackColor = System.Drawing.Color.Transparent;
			this.btnConfiguracio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.btnConfiguracio.BorderRadius = 0;
			this.btnConfiguracio.ButtonText = "Made By Roger";
			this.btnConfiguracio.Cursor = System.Windows.Forms.Cursors.Arrow;
			this.bTransitionLogo.SetDecoration(this.btnConfiguracio, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.btnConfiguracio, BunifuAnimatorNS.DecorationType.None);
			this.btnConfiguracio.DisabledColor = System.Drawing.Color.Gray;
			this.btnConfiguracio.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.btnConfiguracio.Iconcolor = System.Drawing.Color.Transparent;
			this.btnConfiguracio.Iconimage = null;
			this.btnConfiguracio.Iconimage_right = null;
			this.btnConfiguracio.Iconimage_right_Selected = null;
			this.btnConfiguracio.Iconimage_Selected = null;
			this.btnConfiguracio.IconMarginLeft = 5;
			this.btnConfiguracio.IconMarginRight = 0;
			this.btnConfiguracio.IconRightVisible = true;
			this.btnConfiguracio.IconRightZoom = 0D;
			this.btnConfiguracio.IconVisible = true;
			this.btnConfiguracio.IconZoom = 80D;
			this.btnConfiguracio.IsTab = false;
			this.btnConfiguracio.Location = new System.Drawing.Point(0, 544);
			this.btnConfiguracio.Margin = new System.Windows.Forms.Padding(5);
			this.btnConfiguracio.Name = "btnConfiguracio";
			this.btnConfiguracio.Normalcolor = System.Drawing.Color.Transparent;
			this.btnConfiguracio.OnHovercolor = System.Drawing.Color.Transparent;
			this.btnConfiguracio.OnHoverTextColor = System.Drawing.Color.White;
			this.btnConfiguracio.selected = false;
			this.btnConfiguracio.Size = new System.Drawing.Size(250, 45);
			this.btnConfiguracio.TabIndex = 9;
			this.btnConfiguracio.Text = "Made By Roger";
			this.btnConfiguracio.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnConfiguracio.Textcolor = System.Drawing.Color.White;
			this.btnConfiguracio.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			// 
			// btnAgenda
			// 
			this.btnAgenda.Activecolor = System.Drawing.Color.Transparent;
			this.btnAgenda.BackColor = System.Drawing.Color.Transparent;
			this.btnAgenda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.btnAgenda.BorderRadius = 0;
			this.btnAgenda.ButtonText = "Notes";
			this.btnAgenda.Cursor = System.Windows.Forms.Cursors.Hand;
			this.bTransitionLogo.SetDecoration(this.btnAgenda, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.btnAgenda, BunifuAnimatorNS.DecorationType.None);
			this.btnAgenda.DisabledColor = System.Drawing.Color.Gray;
			this.btnAgenda.Dock = System.Windows.Forms.DockStyle.Top;
			this.btnAgenda.Iconcolor = System.Drawing.Color.Transparent;
			this.btnAgenda.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAgenda.Iconimage")));
			this.btnAgenda.Iconimage_right = null;
			this.btnAgenda.Iconimage_right_Selected = null;
			this.btnAgenda.Iconimage_Selected = null;
			this.btnAgenda.IconMarginLeft = 5;
			this.btnAgenda.IconMarginRight = 0;
			this.btnAgenda.IconRightVisible = true;
			this.btnAgenda.IconRightZoom = 0D;
			this.btnAgenda.IconVisible = true;
			this.btnAgenda.IconZoom = 80D;
			this.btnAgenda.IsTab = false;
			this.btnAgenda.Location = new System.Drawing.Point(0, 202);
			this.btnAgenda.Name = "btnAgenda";
			this.btnAgenda.Normalcolor = System.Drawing.Color.Transparent;
			this.btnAgenda.OnHovercolor = System.Drawing.Color.DodgerBlue;
			this.btnAgenda.OnHoverTextColor = System.Drawing.Color.White;
			this.btnAgenda.selected = false;
			this.btnAgenda.Size = new System.Drawing.Size(250, 45);
			this.btnAgenda.TabIndex = 8;
			this.btnAgenda.Text = "Notes";
			this.btnAgenda.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.btnAgenda.Textcolor = System.Drawing.Color.White;
			this.btnAgenda.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAgenda.Click += new System.EventHandler(this.btnAgenda_Click);
			// 
			// btnVincular
			// 
			this.btnVincular.Activecolor = System.Drawing.Color.Transparent;
			this.btnVincular.BackColor = System.Drawing.Color.Transparent;
			this.btnVincular.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.btnVincular.BorderRadius = 0;
			this.btnVincular.ButtonText = "Alumne-Assignatura";
			this.btnVincular.Cursor = System.Windows.Forms.Cursors.Hand;
			this.bTransitionLogo.SetDecoration(this.btnVincular, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.btnVincular, BunifuAnimatorNS.DecorationType.None);
			this.btnVincular.DisabledColor = System.Drawing.Color.Gray;
			this.btnVincular.Dock = System.Windows.Forms.DockStyle.Top;
			this.btnVincular.Iconcolor = System.Drawing.Color.Transparent;
			this.btnVincular.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnVincular.Iconimage")));
			this.btnVincular.Iconimage_right = null;
			this.btnVincular.Iconimage_right_Selected = null;
			this.btnVincular.Iconimage_Selected = null;
			this.btnVincular.IconMarginLeft = 5;
			this.btnVincular.IconMarginRight = 0;
			this.btnVincular.IconRightVisible = true;
			this.btnVincular.IconRightZoom = 0D;
			this.btnVincular.IconVisible = true;
			this.btnVincular.IconZoom = 80D;
			this.btnVincular.IsTab = false;
			this.btnVincular.Location = new System.Drawing.Point(0, 157);
			this.btnVincular.Name = "btnVincular";
			this.btnVincular.Normalcolor = System.Drawing.Color.Transparent;
			this.btnVincular.OnHovercolor = System.Drawing.Color.DodgerBlue;
			this.btnVincular.OnHoverTextColor = System.Drawing.Color.White;
			this.btnVincular.selected = false;
			this.btnVincular.Size = new System.Drawing.Size(250, 45);
			this.btnVincular.TabIndex = 7;
			this.btnVincular.Text = "Alumne-Assignatura";
			this.btnVincular.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.btnVincular.Textcolor = System.Drawing.Color.White;
			this.btnVincular.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnVincular.Click += new System.EventHandler(this.btnVincular_Click);
			// 
			// btnAssignatures
			// 
			this.btnAssignatures.Activecolor = System.Drawing.Color.Transparent;
			this.btnAssignatures.BackColor = System.Drawing.Color.Transparent;
			this.btnAssignatures.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.btnAssignatures.BorderRadius = 0;
			this.btnAssignatures.ButtonText = "Assignatures";
			this.btnAssignatures.Cursor = System.Windows.Forms.Cursors.Hand;
			this.bTransitionLogo.SetDecoration(this.btnAssignatures, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.btnAssignatures, BunifuAnimatorNS.DecorationType.None);
			this.btnAssignatures.DisabledColor = System.Drawing.Color.Gray;
			this.btnAssignatures.Dock = System.Windows.Forms.DockStyle.Top;
			this.btnAssignatures.Iconcolor = System.Drawing.Color.Transparent;
			this.btnAssignatures.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAssignatures.Iconimage")));
			this.btnAssignatures.Iconimage_right = null;
			this.btnAssignatures.Iconimage_right_Selected = null;
			this.btnAssignatures.Iconimage_Selected = null;
			this.btnAssignatures.IconMarginLeft = 5;
			this.btnAssignatures.IconMarginRight = 0;
			this.btnAssignatures.IconRightVisible = true;
			this.btnAssignatures.IconRightZoom = 0D;
			this.btnAssignatures.IconVisible = true;
			this.btnAssignatures.IconZoom = 80D;
			this.btnAssignatures.IsTab = false;
			this.btnAssignatures.Location = new System.Drawing.Point(0, 112);
			this.btnAssignatures.Name = "btnAssignatures";
			this.btnAssignatures.Normalcolor = System.Drawing.Color.Transparent;
			this.btnAssignatures.OnHovercolor = System.Drawing.Color.DodgerBlue;
			this.btnAssignatures.OnHoverTextColor = System.Drawing.Color.White;
			this.btnAssignatures.selected = false;
			this.btnAssignatures.Size = new System.Drawing.Size(250, 45);
			this.btnAssignatures.TabIndex = 6;
			this.btnAssignatures.Text = "Assignatures";
			this.btnAssignatures.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.btnAssignatures.Textcolor = System.Drawing.Color.White;
			this.btnAssignatures.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAssignatures.Click += new System.EventHandler(this.btnAssignatures_Click);
			// 
			// btnAlumnes
			// 
			this.btnAlumnes.Activecolor = System.Drawing.Color.Transparent;
			this.btnAlumnes.BackColor = System.Drawing.Color.Transparent;
			this.btnAlumnes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.btnAlumnes.BorderRadius = 0;
			this.btnAlumnes.ButtonText = "Alumnes";
			this.btnAlumnes.Cursor = System.Windows.Forms.Cursors.Hand;
			this.bTransitionLogo.SetDecoration(this.btnAlumnes, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.btnAlumnes, BunifuAnimatorNS.DecorationType.None);
			this.btnAlumnes.DisabledColor = System.Drawing.Color.Gray;
			this.btnAlumnes.Dock = System.Windows.Forms.DockStyle.Top;
			this.btnAlumnes.Iconcolor = System.Drawing.Color.Transparent;
			this.btnAlumnes.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAlumnes.Iconimage")));
			this.btnAlumnes.Iconimage_right = null;
			this.btnAlumnes.Iconimage_right_Selected = null;
			this.btnAlumnes.Iconimage_Selected = null;
			this.btnAlumnes.IconMarginLeft = 5;
			this.btnAlumnes.IconMarginRight = 0;
			this.btnAlumnes.IconRightVisible = true;
			this.btnAlumnes.IconRightZoom = 0D;
			this.btnAlumnes.IconVisible = true;
			this.btnAlumnes.IconZoom = 80D;
			this.btnAlumnes.IsTab = false;
			this.btnAlumnes.Location = new System.Drawing.Point(0, 67);
			this.btnAlumnes.Name = "btnAlumnes";
			this.btnAlumnes.Normalcolor = System.Drawing.Color.Transparent;
			this.btnAlumnes.OnHovercolor = System.Drawing.Color.DodgerBlue;
			this.btnAlumnes.OnHoverTextColor = System.Drawing.Color.White;
			this.btnAlumnes.selected = false;
			this.btnAlumnes.Size = new System.Drawing.Size(250, 45);
			this.btnAlumnes.TabIndex = 5;
			this.btnAlumnes.Text = "Alumnes";
			this.btnAlumnes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.btnAlumnes.Textcolor = System.Drawing.Color.White;
			this.btnAlumnes.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAlumnes.Click += new System.EventHandler(this.btnAlumnes_Click);
			// 
			// panelMenuState
			// 
			this.panelMenuState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
			this.panelMenuState.Controls.Add(this.pictureBox1);
			this.panelMenuState.Controls.Add(this.btnMenu);
			this.panelMenuState.Controls.Add(this.bunifuSeparator1);
			this.bTransitionLogo.SetDecoration(this.panelMenuState, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.panelMenuState, BunifuAnimatorNS.DecorationType.None);
			this.panelMenuState.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelMenuState.Location = new System.Drawing.Point(0, 0);
			this.panelMenuState.Name = "panelMenuState";
			this.panelMenuState.Size = new System.Drawing.Size(250, 67);
			this.panelMenuState.TabIndex = 4;
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox1.BackgroundImage = global::APP_API.Properties.Resources.api_basics;
			this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.bunifuTransition1.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
			this.bTransitionLogo.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(150, 57);
			this.pictureBox1.TabIndex = 3;
			this.pictureBox1.TabStop = false;
			// 
			// btnMenu
			// 
			this.btnMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnMenu.BackColor = System.Drawing.Color.Transparent;
			this.bTransitionLogo.SetDecoration(this.btnMenu, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.btnMenu, BunifuAnimatorNS.DecorationType.None);
			this.btnMenu.Image = ((System.Drawing.Image)(resources.GetObject("btnMenu.Image")));
			this.btnMenu.ImageActive = null;
			this.btnMenu.Location = new System.Drawing.Point(209, 3);
			this.btnMenu.Name = "btnMenu";
			this.btnMenu.Size = new System.Drawing.Size(37, 38);
			this.btnMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.btnMenu.TabIndex = 1;
			this.btnMenu.TabStop = false;
			this.btnMenu.Zoom = 10;
			this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
			// 
			// bunifuSeparator1
			// 
			this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
			this.bTransitionLogo.SetDecoration(this.bunifuSeparator1, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.bunifuSeparator1, BunifuAnimatorNS.DecorationType.None);
			this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.bunifuSeparator1.LineThickness = 2;
			this.bunifuSeparator1.Location = new System.Drawing.Point(0, 60);
			this.bunifuSeparator1.Name = "bunifuSeparator1";
			this.bunifuSeparator1.Size = new System.Drawing.Size(252, 10);
			this.bunifuSeparator1.TabIndex = 0;
			this.bunifuSeparator1.Transparency = 255;
			this.bunifuSeparator1.Vertical = false;
			// 
			// panelTopWindow
			// 
			this.panelTopWindow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTopWindow.BackgroundImage")));
			this.panelTopWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.panelTopWindow.Controls.Add(this.btnbtnMinimized);
			this.panelTopWindow.Controls.Add(this.btnMaximized);
			this.panelTopWindow.Controls.Add(this.btnExit);
			this.bTransitionLogo.SetDecoration(this.panelTopWindow, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.panelTopWindow, BunifuAnimatorNS.DecorationType.None);
			this.panelTopWindow.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTopWindow.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(75)))), ((int)(((byte)(255)))));
			this.panelTopWindow.GradientBottomRight = System.Drawing.Color.Blue;
			this.panelTopWindow.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(75)))), ((int)(((byte)(255)))));
			this.panelTopWindow.GradientTopRight = System.Drawing.Color.Blue;
			this.panelTopWindow.Location = new System.Drawing.Point(0, 0);
			this.panelTopWindow.Name = "panelTopWindow";
			this.panelTopWindow.Quality = 10;
			this.panelTopWindow.Size = new System.Drawing.Size(1450, 32);
			this.panelTopWindow.TabIndex = 0;
			// 
			// btnbtnMinimized
			// 
			this.btnbtnMinimized.BackColor = System.Drawing.Color.Transparent;
			this.bTransitionLogo.SetDecoration(this.btnbtnMinimized, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.btnbtnMinimized, BunifuAnimatorNS.DecorationType.None);
			this.btnbtnMinimized.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnbtnMinimized.Image = global::APP_API.Properties.Resources.minimizeWindow;
			this.btnbtnMinimized.ImageActive = null;
			this.btnbtnMinimized.Location = new System.Drawing.Point(1354, 0);
			this.btnbtnMinimized.Name = "btnbtnMinimized";
			this.btnbtnMinimized.Size = new System.Drawing.Size(32, 32);
			this.btnbtnMinimized.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.btnbtnMinimized.TabIndex = 5;
			this.btnbtnMinimized.TabStop = false;
			this.btnbtnMinimized.Zoom = 10;
			this.btnbtnMinimized.Click += new System.EventHandler(this.btnMinimize_Click);
			// 
			// btnMaximized
			// 
			this.btnMaximized.BackColor = System.Drawing.Color.Transparent;
			this.bTransitionLogo.SetDecoration(this.btnMaximized, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.btnMaximized, BunifuAnimatorNS.DecorationType.None);
			this.btnMaximized.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnMaximized.Image = global::APP_API.Properties.Resources.maximizeWindow;
			this.btnMaximized.ImageActive = null;
			this.btnMaximized.Location = new System.Drawing.Point(1386, 0);
			this.btnMaximized.Name = "btnMaximized";
			this.btnMaximized.Size = new System.Drawing.Size(32, 32);
			this.btnMaximized.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.btnMaximized.TabIndex = 4;
			this.btnMaximized.TabStop = false;
			this.btnMaximized.Zoom = 10;
			this.btnMaximized.Click += new System.EventHandler(this.btnMaximize_Click);
			// 
			// btnExit
			// 
			this.btnExit.BackColor = System.Drawing.Color.Transparent;
			this.bTransitionLogo.SetDecoration(this.btnExit, BunifuAnimatorNS.DecorationType.None);
			this.bunifuTransition1.SetDecoration(this.btnExit, BunifuAnimatorNS.DecorationType.None);
			this.btnExit.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnExit.Image = global::APP_API.Properties.Resources.closeWindow;
			this.btnExit.ImageActive = null;
			this.btnExit.Location = new System.Drawing.Point(1418, 0);
			this.btnExit.Name = "btnExit";
			this.btnExit.Size = new System.Drawing.Size(32, 32);
			this.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.btnExit.TabIndex = 3;
			this.btnExit.TabStop = false;
			this.btnExit.Zoom = 10;
			this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
			// 
			// bWindowDrag
			// 
			this.bWindowDrag.Fixed = true;
			this.bWindowDrag.Horizontal = true;
			this.bWindowDrag.TargetControl = this.panelTopWindow;
			this.bWindowDrag.Vertical = true;
			// 
			// bTransitionLogo
			// 
			this.bTransitionLogo.AnimationType = BunifuAnimatorNS.AnimationType.ScaleAndRotate;
			this.bTransitionLogo.Cursor = null;
			animation2.AnimateOnlyDifferences = true;
			animation2.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.BlindCoeff")));
			animation2.LeafCoeff = 0F;
			animation2.MaxTime = 1F;
			animation2.MinTime = 0F;
			animation2.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicCoeff")));
			animation2.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicShift")));
			animation2.MosaicSize = 0;
			animation2.Padding = new System.Windows.Forms.Padding(30);
			animation2.RotateCoeff = 0.5F;
			animation2.RotateLimit = 0.2F;
			animation2.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.ScaleCoeff")));
			animation2.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.SlideCoeff")));
			animation2.TimeCoeff = 0F;
			animation2.TransparencyCoeff = 0F;
			this.bTransitionLogo.DefaultAnimation = animation2;
			this.bTransitionLogo.MaxAnimationTime = 3000;
			this.bTransitionLogo.TimeStep = 0.01F;
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.SkyBlue;
			this.ClientSize = new System.Drawing.Size(1450, 621);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.PanelLeftMenu);
			this.Controls.Add(this.panelTopWindow);
			this.bunifuTransition1.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
			this.bTransitionLogo.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "frmMain";
			this.PanelLeftMenu.ResumeLayout(false);
			this.panelMenuState.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnMenu)).EndInit();
			this.panelTopWindow.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btnbtnMinimized)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnMaximized)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private Bunifu.Framework.UI.BunifuGradientPanel panelTopWindow;
		private Bunifu.Framework.UI.BunifuImageButton btnMaximized;
		private Bunifu.Framework.UI.BunifuImageButton btnExit;
		private Bunifu.Framework.UI.BunifuGradientPanel PanelLeftMenu;
		private BunifuAnimatorNS.BunifuTransition bunifuTransition1;
		private Bunifu.Framework.UI.BunifuFlatButton btnAlumnes;
		private System.Windows.Forms.Panel panelMenuState;
		private Bunifu.Framework.UI.BunifuImageButton btnMenu;
		private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
		private Bunifu.Framework.UI.BunifuDragControl bWindowDrag;
		private Bunifu.Framework.UI.BunifuImageButton btnbtnMinimized;
		private Bunifu.Framework.UI.BunifuFlatButton btnAssignatures;
		private Bunifu.Framework.UI.BunifuFlatButton btnVincular;
		private Bunifu.Framework.UI.BunifuFlatButton btnAgenda;
		private System.Windows.Forms.PictureBox pictureBox1;
		private BunifuAnimatorNS.BunifuTransition bTransitionLogo;
		private Bunifu.Framework.UI.BunifuFlatButton btnConfiguracio;
		private System.Windows.Forms.Panel panel1;
		private Bunifu.Framework.UI.BunifuFlatButton btnBunExit;
	}
}

