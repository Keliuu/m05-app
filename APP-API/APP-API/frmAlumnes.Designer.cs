﻿namespace APP_API
{
	partial class frmAlumnes
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.btnGet = new System.Windows.Forms.Button();
			this.btnPost = new System.Windows.Forms.Button();
			this.btnSelectArxiu = new System.Windows.Forms.Button();
			this.dgvJson = new System.Windows.Forms.DataGridView();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btnBuscarId = new System.Windows.Forms.Button();
			this.txtGetId = new System.Windows.Forms.TextBox();
			this.btnDelete = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.txtAluNom = new System.Windows.Forms.TextBox();
			this.txtAluCognom = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtAluEmail = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.dgvAssignatures = new System.Windows.Forms.DataGridView();
			this.label5 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.dgvJson)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAssignatures)).BeginInit();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(18, 55);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(456, 20);
			this.textBox1.TabIndex = 0;
			// 
			// btnGet
			// 
			this.btnGet.Location = new System.Drawing.Point(6, 23);
			this.btnGet.Name = "btnGet";
			this.btnGet.Size = new System.Drawing.Size(75, 23);
			this.btnGet.TabIndex = 1;
			this.btnGet.Text = "GET";
			this.btnGet.UseVisualStyleBackColor = true;
			this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
			// 
			// btnPost
			// 
			this.btnPost.Location = new System.Drawing.Point(99, 23);
			this.btnPost.Name = "btnPost";
			this.btnPost.Size = new System.Drawing.Size(75, 23);
			this.btnPost.TabIndex = 2;
			this.btnPost.Text = "POST";
			this.btnPost.UseVisualStyleBackColor = true;
			this.btnPost.Click += new System.EventHandler(this.btnPost_Click);
			// 
			// btnSelectArxiu
			// 
			this.btnSelectArxiu.Location = new System.Drawing.Point(480, 55);
			this.btnSelectArxiu.Name = "btnSelectArxiu";
			this.btnSelectArxiu.Size = new System.Drawing.Size(135, 23);
			this.btnSelectArxiu.TabIndex = 4;
			this.btnSelectArxiu.Text = "Seleccionar Arxiu Json";
			this.btnSelectArxiu.UseVisualStyleBackColor = true;
			this.btnSelectArxiu.Click += new System.EventHandler(this.btnSelectArxiu_Click);
			// 
			// dgvJson
			// 
			this.dgvJson.AllowUserToAddRows = false;
			this.dgvJson.AllowUserToDeleteRows = false;
			this.dgvJson.AllowUserToResizeColumns = false;
			this.dgvJson.AllowUserToResizeRows = false;
			this.dgvJson.BackgroundColor = System.Drawing.Color.White;
			this.dgvJson.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvJson.Location = new System.Drawing.Point(18, 96);
			this.dgvJson.Name = "dgvJson";
			this.dgvJson.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvJson.Size = new System.Drawing.Size(673, 442);
			this.dgvJson.TabIndex = 6;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.btnBuscarId);
			this.groupBox1.Controls.Add(this.txtGetId);
			this.groupBox1.Controls.Add(this.btnDelete);
			this.groupBox1.Controls.Add(this.btnGet);
			this.groupBox1.Controls.Add(this.btnPost);
			this.groupBox1.Location = new System.Drawing.Point(697, 55);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(475, 62);
			this.groupBox1.TabIndex = 7;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Controls API";
			// 
			// btnBuscarId
			// 
			this.btnBuscarId.Location = new System.Drawing.Point(385, 23);
			this.btnBuscarId.Name = "btnBuscarId";
			this.btnBuscarId.Size = new System.Drawing.Size(84, 23);
			this.btnBuscarId.TabIndex = 5;
			this.btnBuscarId.Text = "Buscar Per ID";
			this.btnBuscarId.UseVisualStyleBackColor = true;
			this.btnBuscarId.Click += new System.EventHandler(this.btnBuscarId_Click);
			// 
			// txtGetId
			// 
			this.txtGetId.Location = new System.Drawing.Point(326, 25);
			this.txtGetId.Name = "txtGetId";
			this.txtGetId.Size = new System.Drawing.Size(50, 20);
			this.txtGetId.TabIndex = 4;
			// 
			// btnDelete
			// 
			this.btnDelete.Enabled = false;
			this.btnDelete.Location = new System.Drawing.Point(191, 23);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(75, 23);
			this.btnDelete.TabIndex = 3;
			this.btnDelete.Text = "DELETE";
			this.btnDelete.UseVisualStyleBackColor = true;
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(616, 55);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 8;
			this.button1.Text = "Reset DGV";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(119, 31);
			this.label1.TabIndex = 15;
			this.label1.Text = "Alumnes";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.dgvAssignatures);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.txtAluEmail);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.txtAluCognom);
			this.groupBox2.Controls.Add(this.txtAluNom);
			this.groupBox2.Location = new System.Drawing.Point(697, 123);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(475, 415);
			this.groupBox2.TabIndex = 18;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Informació Alumne";
			// 
			// txtAluNom
			// 
			this.txtAluNom.Location = new System.Drawing.Point(41, 25);
			this.txtAluNom.Name = "txtAluNom";
			this.txtAluNom.Size = new System.Drawing.Size(156, 20);
			this.txtAluNom.TabIndex = 0;
			// 
			// txtAluCognom
			// 
			this.txtAluCognom.Location = new System.Drawing.Point(278, 25);
			this.txtAluCognom.Name = "txtAluCognom";
			this.txtAluCognom.Size = new System.Drawing.Size(176, 20);
			this.txtAluCognom.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 28);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(29, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Nom";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(226, 28);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(46, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Cognom";
			// 
			// txtAluEmail
			// 
			this.txtAluEmail.Location = new System.Drawing.Point(44, 51);
			this.txtAluEmail.Name = "txtAluEmail";
			this.txtAluEmail.Size = new System.Drawing.Size(410, 20);
			this.txtAluEmail.TabIndex = 4;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 54);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(32, 13);
			this.label4.TabIndex = 5;
			this.label4.Text = "Email";
			// 
			// dgvAssignatures
			// 
			this.dgvAssignatures.AllowUserToAddRows = false;
			this.dgvAssignatures.AllowUserToDeleteRows = false;
			this.dgvAssignatures.AllowUserToResizeColumns = false;
			this.dgvAssignatures.AllowUserToResizeRows = false;
			this.dgvAssignatures.BackgroundColor = System.Drawing.Color.White;
			this.dgvAssignatures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAssignatures.Location = new System.Drawing.Point(9, 96);
			this.dgvAssignatures.Name = "dgvAssignatures";
			this.dgvAssignatures.ReadOnly = true;
			this.dgvAssignatures.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
			this.dgvAssignatures.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvAssignatures.Size = new System.Drawing.Size(460, 313);
			this.dgvAssignatures.TabIndex = 6;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 80);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(67, 13);
			this.label5.TabIndex = 7;
			this.label5.Text = "Assignatures";
			// 
			// frmAlumnes
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.SkyBlue;
			this.ClientSize = new System.Drawing.Size(1184, 545);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.dgvJson);
			this.Controls.Add(this.btnSelectArxiu);
			this.Controls.Add(this.textBox1);
			this.Name = "frmAlumnes";
			this.Text = "Alumnes";
			((System.ComponentModel.ISupportInitialize)(this.dgvJson)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAssignatures)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button btnGet;
		private System.Windows.Forms.Button btnPost;
		private System.Windows.Forms.Button btnSelectArxiu;
		private System.Windows.Forms.DataGridView dgvJson;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Button btnBuscarId;
		private System.Windows.Forms.TextBox txtGetId;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.DataGridView dgvAssignatures;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtAluEmail;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtAluCognom;
		private System.Windows.Forms.TextBox txtAluNom;
	}
}

