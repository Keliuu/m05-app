﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APP_API
{
	public partial class frmAssignatures : Form
	{
		ApiLink api = new ApiLink();
		public frmAssignatures()
		{
			InitializeComponent();
		}

		private void btnSelArxiu_Click(object sender, EventArgs e)
		{
			dgvJson.Rows.Clear();
			dgvJson.Columns.Clear();
			OpenFileDialog openFileDialog1 = new OpenFileDialog();

			openFileDialog1.InitialDirectory = "H:\\Keliu\\Asix\\M05-Hardware\\practicaExamenFinal";
			//openFileDialog1.Filter = "Database files (*.mdb, *.accdb)|*.mdb;*.accdb";
			openFileDialog1.FilterIndex = 0;
			openFileDialog1.RestoreDirectory = true;

			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				textBox1.Text = openFileDialog1.FileName;
				ReadtxtJson(textBox1.Text);
			}
		}

		public void ReadtxtJson(string path)
		{
			List<assignatures> items;
			using (StreamReader r = new StreamReader(path))
			{
				try
				{
					string json = r.ReadToEnd();
					items = JsonConvert.DeserializeObject<List<assignatures>>(json);
					dgvJson.Columns.Add("nom", "nom");
					dgvJson.Columns.Add("professor", "professor");
					int a = 0;
					if (a == 0)
					{
						for (int i = 0; i < items.Count; i++)
						{
							dgvJson.Rows.Add(items[i].nom, items[i].professor);
						}
					}
					for (int i = 0; i < (dgvJson.Rows.Count - 1); i++)
					{

						if (dgvJson.Rows[i].Cells["nom"].Value == null ||
						   dgvJson.Rows[i].Cells["professor"].Value == null)
						{
							MessageBox.Show("columnes buides, estàs segur que estàs inserint el Json d'assingatures? si es així són necessaris els camps nom i professor.");
							dgvJson.Rows.Clear();
						}
					}
				}
				catch (Exception e)
				{
					MessageBox.Show("Error en el format del JSON");
					textBox1.Text = "";
				}
			}
		}

		private void btnPost_Click(object sender, EventArgs e)
		{
			if (dgvJson.Rows.Count != 0)
			{
				foreach (DataGridViewRow row in dgvJson.Rows)
				{
					if (row.DefaultCellStyle.BackColor != Color.LightBlue)
					{
						assignatures ass = new assignatures();
						ass.nom = row.Cells["nom"].Value.ToString();
						ass.professor = row.Cells["professor"].Value.ToString();
						api.postAssignatura(ass);
					}

				}
				//MessageBox.Show("Has insertat " + postCount.ToString() + " assignatures");
			}
			else
			{
				MessageBox.Show("error, no pots fer post perquè no tens cap registre a inserir");
			}
		}

		private void btnGet_Click(object sender, EventArgs e)
		{
			List<assignatures> items;
			ApiLink api = new ApiLink();
			textBox1.Text = "";
			dgvJson.Rows.Clear();
			MessageBox.Show("Espera uns segons, s'esta realitzant la consulta a assignatures" + api.getAssignatura().ToString());
			items = api.itAss;
			readApiResponse(items);
		}

		public void readApiResponse(List<assignatures> item)
		{
			try
			{
				dgvJson.Columns.Add("nom", "nom");
				dgvJson.Columns.Add("professor", "professor");
				int a = 0;
				if (a == 0)
				{
					for (int i = 0; i < item.Count; i++)
					{
						dgvJson.Rows.Add(item[i].nom, item[i].professor);
						dgvJson.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
					}
				}				
			}
			catch (Exception e)
			{
				MessageBox.Show("Error en el get. Tens l'appi encesa?");
			}
		}
	}
}
