﻿namespace APP_API
{
	partial class frmAssignaturaAlumne
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.dgvAlumnes = new System.Windows.Forms.DataGridView();
			this.txtNomAlumne = new System.Windows.Forms.TextBox();
			this.txtIdAlumne = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.dgvAssignatures = new System.Windows.Forms.DataGridView();
			this.label3 = new System.Windows.Forms.Label();
			this.txtNomAssignatura = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtIdAssignatura = new System.Windows.Forms.TextBox();
			this.btnVincular = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAlumnes)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAssignatures)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.dgvAlumnes);
			this.groupBox1.Controls.Add(this.txtNomAlumne);
			this.groupBox1.Controls.Add(this.txtIdAlumne);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(500, 526);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Alumnes";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(84, 34);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(29, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Nom";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 34);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(16, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Id";
			// 
			// dgvAlumnes
			// 
			this.dgvAlumnes.AllowUserToAddRows = false;
			this.dgvAlumnes.AllowUserToDeleteRows = false;
			this.dgvAlumnes.AllowUserToResizeColumns = false;
			this.dgvAlumnes.AllowUserToResizeRows = false;
			this.dgvAlumnes.BackgroundColor = System.Drawing.Color.White;
			this.dgvAlumnes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAlumnes.Location = new System.Drawing.Point(6, 73);
			this.dgvAlumnes.Name = "dgvAlumnes";
			this.dgvAlumnes.ReadOnly = true;
			this.dgvAlumnes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvAlumnes.Size = new System.Drawing.Size(488, 447);
			this.dgvAlumnes.TabIndex = 0;
			this.dgvAlumnes.SelectionChanged += new System.EventHandler(this.dgvAlumnes_SelectionChanged);
			// 
			// txtNomAlumne
			// 
			this.txtNomAlumne.Location = new System.Drawing.Point(119, 31);
			this.txtNomAlumne.Name = "txtNomAlumne";
			this.txtNomAlumne.Size = new System.Drawing.Size(375, 20);
			this.txtNomAlumne.TabIndex = 3;
			// 
			// txtIdAlumne
			// 
			this.txtIdAlumne.Location = new System.Drawing.Point(28, 31);
			this.txtIdAlumne.Name = "txtIdAlumne";
			this.txtIdAlumne.Size = new System.Drawing.Size(50, 20);
			this.txtIdAlumne.TabIndex = 2;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.dgvAssignatures);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.txtNomAssignatura);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.txtIdAssignatura);
			this.groupBox2.Location = new System.Drawing.Point(672, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(500, 526);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Assignatures";
			// 
			// dgvAssignatures
			// 
			this.dgvAssignatures.AllowUserToAddRows = false;
			this.dgvAssignatures.AllowUserToDeleteRows = false;
			this.dgvAssignatures.AllowUserToResizeColumns = false;
			this.dgvAssignatures.AllowUserToResizeRows = false;
			this.dgvAssignatures.BackgroundColor = System.Drawing.Color.White;
			this.dgvAssignatures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAssignatures.Location = new System.Drawing.Point(6, 73);
			this.dgvAssignatures.Name = "dgvAssignatures";
			this.dgvAssignatures.ReadOnly = true;
			this.dgvAssignatures.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvAssignatures.Size = new System.Drawing.Size(488, 447);
			this.dgvAssignatures.TabIndex = 6;
			this.dgvAssignatures.SelectionChanged += new System.EventHandler(this.dgvAssignatures_SelectionChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(84, 34);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(29, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Nom";
			// 
			// txtNomAssignatura
			// 
			this.txtNomAssignatura.Location = new System.Drawing.Point(119, 31);
			this.txtNomAssignatura.Name = "txtNomAssignatura";
			this.txtNomAssignatura.Size = new System.Drawing.Size(375, 20);
			this.txtNomAssignatura.TabIndex = 7;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 34);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(16, 13);
			this.label4.TabIndex = 8;
			this.label4.Text = "Id";
			// 
			// txtIdAssignatura
			// 
			this.txtIdAssignatura.Location = new System.Drawing.Point(28, 31);
			this.txtIdAssignatura.Name = "txtIdAssignatura";
			this.txtIdAssignatura.Size = new System.Drawing.Size(50, 20);
			this.txtIdAssignatura.TabIndex = 6;
			// 
			// btnVincular
			// 
			this.btnVincular.Location = new System.Drawing.Point(515, 245);
			this.btnVincular.Name = "btnVincular";
			this.btnVincular.Size = new System.Drawing.Size(152, 23);
			this.btnVincular.TabIndex = 6;
			this.btnVincular.Text = "Vincular Assignatura Alumne";
			this.btnVincular.UseVisualStyleBackColor = true;
			this.btnVincular.Click += new System.EventHandler(this.btnVincular_Click);
			// 
			// frmAssignaturaAlumne
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.SkyBlue;
			this.ClientSize = new System.Drawing.Size(1184, 550);
			this.Controls.Add(this.btnVincular);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "frmAssignaturaAlumne";
			this.Text = "Vincular Assignatures Alumnes";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAlumnes)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAssignatures)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataGridView dgvAlumnes;
		private System.Windows.Forms.TextBox txtNomAlumne;
		private System.Windows.Forms.TextBox txtIdAlumne;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.DataGridView dgvAssignatures;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtNomAssignatura;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtIdAssignatura;
		private System.Windows.Forms.Button btnVincular;
	}
}