﻿namespace APP_API
{
	partial class frmNotes
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.dgvAlumnes = new System.Windows.Forms.DataGridView();
			this.label5 = new System.Windows.Forms.Label();
			this.txtAlumneCognom = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtAlumneNom = new System.Windows.Forms.TextBox();
			this.txtAlumneId = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.dgvAssignatures = new System.Windows.Forms.DataGridView();
			this.label6 = new System.Windows.Forms.Label();
			this.txtNomAssignatura = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.txtIdAssignatura = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.txtNota = new System.Windows.Forms.TextBox();
			this.btnAfegirNota = new System.Windows.Forms.Button();
			this.label8 = new System.Windows.Forms.Label();
			this.txtNomProfessor = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAlumnes)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAssignatures)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(86, 31);
			this.label1.TabIndex = 0;
			this.label1.Text = "Notes";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.dgvAlumnes);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.txtAlumneCognom);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.txtAlumneNom);
			this.groupBox1.Controls.Add(this.txtAlumneId);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(12, 57);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(510, 481);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Alumne";
			// 
			// dgvAlumnes
			// 
			this.dgvAlumnes.AllowUserToAddRows = false;
			this.dgvAlumnes.AllowUserToDeleteRows = false;
			this.dgvAlumnes.AllowUserToResizeColumns = false;
			this.dgvAlumnes.AllowUserToResizeRows = false;
			this.dgvAlumnes.BackgroundColor = System.Drawing.Color.White;
			this.dgvAlumnes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAlumnes.Location = new System.Drawing.Point(13, 81);
			this.dgvAlumnes.MultiSelect = false;
			this.dgvAlumnes.Name = "dgvAlumnes";
			this.dgvAlumnes.ReadOnly = true;
			this.dgvAlumnes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvAlumnes.Size = new System.Drawing.Size(483, 375);
			this.dgvAlumnes.TabIndex = 6;
			this.dgvAlumnes.SelectionChanged += new System.EventHandler(this.dgvAlumnes_SelectionChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(289, 37);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(54, 15);
			this.label5.TabIndex = 5;
			this.label5.Text = "Cognom";
			// 
			// txtAlumneCognom
			// 
			this.txtAlumneCognom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtAlumneCognom.Location = new System.Drawing.Point(349, 34);
			this.txtAlumneCognom.Name = "txtAlumneCognom";
			this.txtAlumneCognom.ReadOnly = true;
			this.txtAlumneCognom.Size = new System.Drawing.Size(147, 21);
			this.txtAlumneCognom.TabIndex = 4;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(96, 37);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(34, 15);
			this.label4.TabIndex = 3;
			this.label4.Text = "Nom";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(10, 37);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(17, 15);
			this.label3.TabIndex = 2;
			this.label3.Text = "Id";
			// 
			// txtAlumneNom
			// 
			this.txtAlumneNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtAlumneNom.Location = new System.Drawing.Point(136, 34);
			this.txtAlumneNom.Name = "txtAlumneNom";
			this.txtAlumneNom.ReadOnly = true;
			this.txtAlumneNom.Size = new System.Drawing.Size(147, 21);
			this.txtAlumneNom.TabIndex = 1;
			// 
			// txtAlumneId
			// 
			this.txtAlumneId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtAlumneId.Location = new System.Drawing.Point(33, 34);
			this.txtAlumneId.Name = "txtAlumneId";
			this.txtAlumneId.ReadOnly = true;
			this.txtAlumneId.Size = new System.Drawing.Size(57, 21);
			this.txtAlumneId.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.txtNomProfessor);
			this.groupBox2.Controls.Add(this.label8);
			this.groupBox2.Controls.Add(this.dgvAssignatures);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.txtNomAssignatura);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.txtIdAssignatura);
			this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox2.Location = new System.Drawing.Point(662, 57);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(510, 481);
			this.groupBox2.TabIndex = 2;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Assignatura";
			// 
			// dgvAssignatures
			// 
			this.dgvAssignatures.AllowUserToAddRows = false;
			this.dgvAssignatures.AllowUserToDeleteRows = false;
			this.dgvAssignatures.AllowUserToResizeColumns = false;
			this.dgvAssignatures.AllowUserToResizeRows = false;
			this.dgvAssignatures.BackgroundColor = System.Drawing.Color.White;
			this.dgvAssignatures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAssignatures.Location = new System.Drawing.Point(20, 81);
			this.dgvAssignatures.MultiSelect = false;
			this.dgvAssignatures.Name = "dgvAssignatures";
			this.dgvAssignatures.ReadOnly = true;
			this.dgvAssignatures.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvAssignatures.Size = new System.Drawing.Size(483, 375);
			this.dgvAssignatures.TabIndex = 7;
			this.dgvAssignatures.SelectionChanged += new System.EventHandler(this.dgvAssignatures_SelectionChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(103, 37);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(34, 15);
			this.label6.TabIndex = 10;
			this.label6.Text = "Nom";
			// 
			// txtNomAssignatura
			// 
			this.txtNomAssignatura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtNomAssignatura.Location = new System.Drawing.Point(143, 34);
			this.txtNomAssignatura.Name = "txtNomAssignatura";
			this.txtNomAssignatura.ReadOnly = true;
			this.txtNomAssignatura.Size = new System.Drawing.Size(147, 21);
			this.txtNomAssignatura.TabIndex = 8;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(17, 37);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(17, 15);
			this.label7.TabIndex = 9;
			this.label7.Text = "Id";
			// 
			// txtIdAssignatura
			// 
			this.txtIdAssignatura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtIdAssignatura.Location = new System.Drawing.Point(40, 34);
			this.txtIdAssignatura.Name = "txtIdAssignatura";
			this.txtIdAssignatura.ReadOnly = true;
			this.txtIdAssignatura.Size = new System.Drawing.Size(57, 21);
			this.txtIdAssignatura.TabIndex = 7;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(568, 145);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(43, 20);
			this.label2.TabIndex = 3;
			this.label2.Text = "Nota";
			// 
			// txtNota
			// 
			this.txtNota.Location = new System.Drawing.Point(541, 178);
			this.txtNota.Name = "txtNota";
			this.txtNota.Size = new System.Drawing.Size(98, 20);
			this.txtNota.TabIndex = 4;
			this.txtNota.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// btnAfegirNota
			// 
			this.btnAfegirNota.Location = new System.Drawing.Point(528, 228);
			this.btnAfegirNota.Name = "btnAfegirNota";
			this.btnAfegirNota.Size = new System.Drawing.Size(128, 23);
			this.btnAfegirNota.TabIndex = 5;
			this.btnAfegirNota.Text = "Afegir Nota";
			this.btnAfegirNota.UseVisualStyleBackColor = true;
			this.btnAfegirNota.Click += new System.EventHandler(this.btnAfegirNota_Click);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(297, 37);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(59, 15);
			this.label8.TabIndex = 11;
			this.label8.Text = "Professor";
			// 
			// txtNomProfessor
			// 
			this.txtNomProfessor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtNomProfessor.Location = new System.Drawing.Point(356, 34);
			this.txtNomProfessor.Name = "txtNomProfessor";
			this.txtNomProfessor.ReadOnly = true;
			this.txtNomProfessor.Size = new System.Drawing.Size(147, 21);
			this.txtNomProfessor.TabIndex = 7;
			// 
			// frmNotes
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.SkyBlue;
			this.ClientSize = new System.Drawing.Size(1184, 550);
			this.Controls.Add(this.btnAfegirNota);
			this.Controls.Add(this.txtNota);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label1);
			this.Name = "frmNotes";
			this.Text = "Notes";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAlumnes)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAssignatures)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.DataGridView dgvAlumnes;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtAlumneCognom;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtAlumneNom;
		private System.Windows.Forms.TextBox txtAlumneId;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.DataGridView dgvAssignatures;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtNomAssignatura;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtIdAssignatura;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtNota;
		private System.Windows.Forms.Button btnAfegirNota;
		private System.Windows.Forms.TextBox txtNomProfessor;
		private System.Windows.Forms.Label label8;
	}
}